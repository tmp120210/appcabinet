'use strict';

/**
 * @ngdoc overview
 * @name yapp
 * @description
 * # yapp
 *
 * Main module of the application.
 */
angular
  .module('yapp', [
    'ui.router',
    'ngAnimate'
  ])
  .config(function($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.when('/dashboard', '/dashboard/overview');
    $urlRouterProvider.otherwise('/dashboard');

    $stateProvider
      .state('base', {
        abstract: true,
        url: '',
        templateUrl: 'views/base.html'
      })
        .state('login', {
          url: '/login',
          parent: 'base',
          templateUrl: 'views/login.html',
          controller: 'LoginCtrl',
          authRequired : false
        })
        .state('signup', {
          url: '/signup',
          parent: 'base',
          templateUrl: 'views/signup.html',
          controller: 'SignupCtrl',
          authRequired : false
        })
        .state('dashboard', {
          url: '/dashboard',
          parent: 'base',
          templateUrl: 'views/dashboard.html',
          controller: 'DashboardCtrl',
          authRequired : true
        })
          .state('overview', {
            url: '/overview',
            parent: 'dashboard',
            templateUrl: 'views/dashboard/overview.html',
        	authRequired : true
          })
          .state('reports', {
            url: '/reports',
            parent: 'dashboard',
            templateUrl: 'views/dashboard/reports.html',
        	authRequired : true
          });

  }).run(function($rootScope, $location){
	  Parse.initialize("DPCIxlPTn0Uzlrx4aBXKHDqYQLv2SOQyv85zZa0E", "7225MJ23TzMXJGxKMnBwwoufy7VlU7EXI8ih9yJT");
	  
	  $rootScope.$on('$stateChangeStart',
		  function (event, toState, toParams, fromState, fromParams) {
			  if(!Parse.User.current() && toState.authRequired){
	//		    	event.preventDefault();
				  console.log("force to login page");
			      $location.path('/login');
			  }
			  if(Parse.User.current() && !toState.authRequired){
				  console.log("force to dash page");
				  $location.path('/login/dashboard');
			  }
		  }
	  );
	  
  }).directive('dropzone', function () {
	  return function (scope, element, attrs) {
		    var config, dropzone;

		    config = {'options' : {
		    	'url' : '/post/upload'
		    }}

		    // create a Dropzone for the element with the given options
		    dropzone = new Dropzone(element[0], config.options);

		    // bind the given event handlers
		    angular.forEach(config.eventHandlers, function (handler, event) {
		      dropzone.on(event, handler);
		    });
		  };	
	  });
