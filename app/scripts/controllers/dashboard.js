'use strict';

/**
 * @ngdoc function
 * @name yapp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of yapp
 */
angular.module('yapp')
  .controller('DashboardCtrl', function($scope, $state, $location) {

    $scope.$state = $state;

    
    $scope.logout = function(){
    	Parse.User.logOut();
    	$location.path('/login');
    }
  });
