'use strict';

/**
 * @ngdoc function
 * @name yapp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of yapp
 */
angular.module('yapp')
  .controller('LoginCtrl', function($scope, $location) {

    $scope.submit = function() {

    	Parse.User.logIn($scope.user.email, $scope.user.password, {
		  success: function(user) {
			  console.log("user logged in")
			  $scope.$apply(function(){
				  $location.path('/dashboard');  
			  });
		      
		  },
		  error: function(user, error) {
			  alert("Неверный логин и пароль!")
		    // The login failed. Check error to see why.
		  }
		});
    	

      return false;
    }

  });
