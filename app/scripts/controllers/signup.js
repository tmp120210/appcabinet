'use strict';

/**
 * @ngdoc function
 * @name yapp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of yapp
 */
angular.module('yapp')
  .controller('SignupCtrl', function($scope, $location, $state) {

    $scope.submit = function() {
    	var user = new Parse.User();
    	alert(JSON.stringify($scope.user));
    	user.set("username", $scope.user.email);
    	user.set("email", $scope.user.email);
    	user.set("password", $scope.user.password);

    	user.signUp(null, {
    	  success: function(user) {
    		  $scope.$apply(function(){
				  $location.path('/dashboard');  
			  });
    	  },
    	  error: function(user, error) {
    	    // Show the error message somewhere and let the user try again.
    	    alert("Error: " + error.code + " " + error.message);
    	  }
    	});

      return false;
    }

  });
